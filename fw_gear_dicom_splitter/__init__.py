"""The fw_gear_dicom_splitter package."""

from importlib import metadata

pkg_name = __package__
__version__ = metadata.version(__package__)

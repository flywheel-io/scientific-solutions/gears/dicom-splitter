FROM flywheel/python:3.11-alpine AS base

SHELL ["/bin/bash", "-euxo", "pipefail", "-c"]
WORKDIR /src
ARG V_GDCM=3.0.24
RUN apk update && apk upgrade && apk --no-cache add -tbuild-deps \
        build-base cmake swig xh; \
    xh -IF "https://github.com/malaterre/GDCM/archive/refs/tags/v$V_GDCM.tar.gz" | tar xz; \
    cmake "GDCM-$V_GDCM" \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_C_FLAGS="-fPIC -D_LARGEFILE64_SOURCE" \
        -DCMAKE_CXX_FLAGS="-fPIC -D_LARGEFILE64_SOURCE" \
        -DGDCM_BUILD_APPLICATIONS=ON \
        -DGDCM_BUILD_SHARED_LIBS=ON \
        -DGDCM_INSTALL_PYTHONMODULE_DIR="$PYTHONPATH" \
        -DGDCM_WRAP_PYTHON=ON \
    ; \
    make install -j"$(nproc)"; \
    rm -rf /src/*; \
    gdcmdump --help; \
    python -c "import gdcm"

ENV FLYWHEEL="/flywheel/v0"
WORKDIR ${FLYWHEEL}

ENTRYPOINT ["python","/flywheel/v0/run.py"]


FROM base AS build
# Install build dependencies
RUN apk add --no-cache --virtual .build-deps \
    build-base \
    gfortran \
    musl-dev \
    freetype-dev \
    libpng-dev \
    openblas-dev \
    openjpeg-dev

COPY requirements.txt $FLYWHEEL/
RUN uv pip install -r requirements.txt


FROM build AS dev
COPY requirements-dev.txt ./
RUN uv pip install -rrequirements-dev.txt
COPY . .
RUN uv pip install --no-deps -e.


FROM base
RUN apk add --no-cache \
    openjpeg \
    libstdc++ \
    libgfortran \
    openblas \
    freetype \
    libpng

COPY --from=build /venv /venv
COPY . .
RUN uv pip install --no-deps -e.
    
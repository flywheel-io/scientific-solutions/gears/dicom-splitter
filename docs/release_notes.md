# Release Notes

## 2.1.0

__Enhancement__:

* Add filter to catch non-DICOM files that may be present in the DICOM archive
  * Filter is default on but can be configured off with `filter_archive=False`

__Maintenance__:

* Update `fw-file` to 4.0.0
  * Includes `pydicom` update to 3.0.1
* Update README
* Change gear args to be passed as a dataclass instead of a tuple of expanding length

## 2.0.3

__Enhancement__:

* Multiple SeriesNumber values within a split now logs a warning and continues,
instead of erroring and telling the user to split on SeriesNumber
* Updated README config options to match current options
* Improved docstrings

## 2.0.1

__Fix__:

* *Do* perform split checks even if the SOPClassUID is secondary capture image storage.

## 2.0.0

__Enhancement__:

* Breaking change: rename gear to dicom-splitter

## 1.7.2

__Maintenance__:

* Remove unneeded importlib-metadata dependency
* Add gear categories to manifest

## 1.7.1

__Maintenance__:

* Upgrade flywheel-gear-toolkit dependency

## 1.7.0

__Enhancement__:

* Change gear name in manifest to "dicom-splitter"
* Allow for input dicoms lacking NumberOfFrames element. See GEAR-5170
* Include split-on elements in output filename
* Clarify confusing error messages
* Do not change SeriesInstanceUID when splitting on SeriesInstanceUID
* Mark gear run as PASS in qc info if split not necessary

__Maintenance__:

* Upgrade fw-file dependency

## 1.5.2

__Fix__:

* Handle multiple values for SOPClassUID secondary capture image storage. See GEAR-4598.

## 1.5.1

__Fix__:

* Do not split if the SOPClassUID is secondary capture image storage. See GEAR-4347.

## 1.4.3

__Maintenance__:

* Looser python version control

## 1.4.2

__Enhancement__:

* Pull in updates from fw-file
  * Improve handling of invalid CS values
  * Improve handling of invalid DS/IS values

## 1.4.1

__Enhancement__:

* Add an option to add an additional tag to a single output for gear-rule triggering

## 1.4.0

__Enhancement__:

* Output split dicoms will have unique SeriesInstanceUIDs

## 1.3.1

__Maintenance__:

* Change `zip_single` config option to `zip-single-dicom` for consistency
  with dicom-fixer, also change values to `match` or `no` instead of
  boolean.

## 1.3.0

__Enhancement__:

* Add config option to control zipping of single dicoms
* Update to new metadata methods to improve logging of metadata

## 1.2.5

__Bug Fix__:

* Rework euclidean splitter decision
  * Add rounding to result of euclidean distance to reduce
      sensitivity to very small changes in distance
  * Change to halfnorm dist with RV being abs(distance from mean)

## 1.2.4

__Enhancements__:

* Add error handling for failures within a splitter.

## 1.2.3

__Maintenance__:

* !24 (merged) MAIN: Catch warnings for RuntimeErrors already handled

## 1.2.2

__Maintenance__:

* Only tag when tag isn't present
* Add env into manifest
* Update fw-file version

## 1.2.1

__Documentation__:

* Publish README to PyPI

## 1.2.0

__Maintenance__:

* Upgrade to qa-ci, update gear-toolkit and fw-file versions.

__Fix__:

* Helpful error message and graceful exit when non-zip file.
* Fix group-by logic to be simpler and more robust

## 1.1.2

__Maintenance__:

* Update manifest description and URL

## 1.1.1

__Fix__:

* Tagging input file for deletion if outputs created.

## 1.1.0

__Fix__:

* Fix output naming when splitting with group_by AND localizer
* Fix setting "deleted" tag on input if split.
* Catch AttributeError for PixelData on Jensen-Shannon splitter and move on.
* Update fw-file with version that handles deid dates of 0001-01-01

## 1.0.0

__Enhancements__:

* Add qc information to output files.
* Update output filenames for split-by to follow the same pattern of: `{SeriesNumber}_{Modality}_{SeriesDescription}_{count}`.
* Improve `SeriesNumber` naming to default to what is present and error
if multiple are present in each unique collection.
* Fix naming of localizer outputs and generation of new SeriesNumber.
* Add config option `tag` to tag output file.

__Bug Fix__:

* Allow splitter to work with extensions other than `.dicom.zip`.
* Don't attempt to split localizer from a collection with less than 2 frames.
* Catch fit solver error for dark images on Jensen-Shannon splitter.

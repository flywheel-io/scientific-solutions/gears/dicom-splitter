#!/usr/bin/env python
"""The run script"""

import logging
import sys
from pathlib import Path

from flywheel.rest import ApiException
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_dicom_splitter.main import run
from fw_gear_dicom_splitter.metadata import populate_qc, populate_tags
from fw_gear_dicom_splitter.parser import parse_config
from fw_gear_dicom_splitter.utils import delete_input

log = logging.getLogger(__name__)


def main(context: GearToolkitContext) -> int:  # pragma: no cover
    """Parses config and run"""
    gear_args = parse_config(context)

    save_paths, success = run(gear_args)

    # set_deleted affects how tags are populated on the input file with populate_tags()
    # If the DICOM was not split, set_deleted==None and the input file is tagged.
    # If the DICOM was split, input is either "deleted" or "retained".
    # If "deleted", no tags are added to the input file (because it's deleted).
    # If "retained", input file is tagged with the configured tags as well as "delete",
    # to match the gear functioning before gear rules read/write was available.
    set_deleted = None
    if save_paths:
        output_msg = "\n".join([str(p) for p in save_paths])
        log.info(f"Found outputs: \n{output_msg}")
        for path in save_paths:
            file_ = Path(path).name
            log.info(f"Adding qc info for {file_}")
            populate_qc(context, file_, True, True)
            # Because output always has either .dicom or .dicom.zip as
            # a suffix, Flywheel should be able to set type without
            # this update, this is just a safeguard...
            context.metadata.update_file_metadata(file_, type="dicom")
        if gear_args.delete_input:
            try:
                log.info("Deleting input")
                delete_input(context)
                set_deleted = "deleted"
            except ApiException as e:
                if e.status == 403:
                    log.exception(
                        "Unable to delete input. \
                                Likely insufficient permissions..."
                    )
                else:
                    log.exception("Unable to delete input.")
                log.info("Input retained")
                set_deleted = "retained"
        else:
            log.info("Input retained")
            set_deleted = "retained"

    elif success:
        # Splitter ran correctly, but found nothing to split
        log.info("DICOM not split, nothing to split on found.")
        populate_qc(context, gear_args.dicom_path.name, False, True)
    else:
        # If it failed, mark that on the input file.
        log.info("DICOM not split due to error.")
        populate_qc(context, gear_args.dicom_path.name, False, False)
        return 1

    # Update tags
    populate_tags(context, save_paths, set_deleted=set_deleted)

    return 0


if __name__ == "__main__":  # pragma: no cover
    # TODO: Remove `fail_on_validation=False` when the engine
    #   .metadata.json schema is updated to allow None values.
    with GearToolkitContext(fail_on_validation=False) as gear_context:
        gear_context.init_logging()
        ret = main(gear_context)
    sys.exit(ret)

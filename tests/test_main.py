import logging
import shutil
import tempfile
from pathlib import Path
from unittest.mock import MagicMock, PropertyMock

import numpy as np
import pandas as pd
import pydicom
import pytest
from fw_file.dicom import DICOM, DICOMCollection

from fw_gear_dicom_splitter.main import (
    gen_split_score,
    run,
    run_individual_split,
    run_split_localizer,
    split_dicom,
)
from fw_gear_dicom_splitter.parser import GearArgs
from fw_gear_dicom_splitter.splitters import (
    EuclideanSplitter,
    JensenShannonDICOMSplitter,
    UniqueTagSingleSplitter,
)

ASSETS_ROOT = Path(__file__).parent / "assets"
DICOM_ROOT = ASSETS_ROOT / "DICOM"


def _get_asset_file(root_dir, filename):
    """Returns path to file in assets."""
    if not isinstance(filename, Path):
        filename = Path(filename)
    path = Path(tempfile.mkdtemp())
    shutil.copy(root_dir / filename, path / filename)
    return str(path / filename)


def dicom_collection(filename):
    file_ = _get_asset_file(DICOM_ROOT, filename)
    if "zip" in filename:
        return DICOMCollection.from_zip(file_)
    dcm = DICOM(file_)
    collection = DICOMCollection()
    collection.append(dcm)
    return collection


@pytest.mark.parametrize(
    "splitter_cls, coll_args, splittercls_args,split_kwargs",
    [
        (
            EuclideanSplitter,
            {
                "test1": {"ImageOrientationPatient": ("DS", [0, 0, 0, 0, 0, 0])},
                "test2": {"ImageOrientationPatient": ("DS", [0, 0, 0, 0, 0, 0])},
                "test3": {"ImageOrientationPatient": ("DS", [0, 0, 0, 0, 0, 0])},
                "test4": {"ImageOrientationPatient": ("DS", [0, 0, 0, 0, 0, 2])},
            },
            {},
            {"thresh": 0.1},
        ),
        (
            UniqueTagSingleSplitter,
            {
                "test1": {"StudyInstanceUID": "0"},
                "test2": {"StudyInstanceUID": "0"},
                "test3": {"StudyInstanceUID": "0"},
                "test4": {"StudyInstanceUID": "1"},
            },
            {"tags": ["StudyInstanceUID"]},
            {},
        ),
    ],
)
def test_individual_split(
    dcmcoll, splitter_cls, coll_args, splittercls_args, split_kwargs
):
    coll = dcmcoll(**coll_args)
    splitter = splitter_cls(coll, 5, **splittercls_args)
    df = pd.DataFrame.from_records([{"path": dcm.filepath} for dcm in coll])
    df["score"] = 0
    run_individual_split(splitter, df, **split_kwargs)

    assert list(df["score"].values) == [0.0, 0.0, 0.0, 5.0]


def test_individual_split_err_no_pix(dcmcoll, mocker, caplog):
    caplog.set_level(logging.INFO)
    mock = PropertyMock()
    mock.side_effect = ValueError("som")
    mocker.patch.object(pydicom.dataset.FileDataset, "pixel_array", mock)
    coll = dcmcoll(**{"test1": {}, "test2": {}, "test3": {}})
    df = pd.DataFrame.from_records([{"path": dcm.filepath} for dcm in coll])
    df["score"] = 0
    splitter = JensenShannonDICOMSplitter(coll)
    run_individual_split(splitter, df)
    exp = [
        (
            "fw_gear_dicom_splitter.main",
            40,
            "Could not run JensenShannonDICOMSplitter: som",
        ),
        ("fw_gear_dicom_splitter.main", 40, "Enable debug to see full stack"),
        (
            "fw_gear_dicom_splitter.main",
            20,
            "Note: JensenShannon splitter is not necessary, but this failure "
            "indicates there is probably something wrong with your DICOM's "
            "PixelData or PixelData attributes.",
        ),
        # DEBUG level not shown given caplog set_level above
        # (
        #    "fw_gear_dicom_splitter.main",
        #    10,
        #    "SplitterError('Could not run JensenShannonDICOMSplitter: som')",
        # ),
    ]
    assert caplog.record_tuples == exp


def test_individual_split_err_empty_pix(dcmcoll, mocker, caplog):
    caplog.set_level(logging.INFO)
    mock = PropertyMock()
    mock.side_effect = TypeError("som")
    mocker.patch.object(pydicom.dataset.FileDataset, "pixel_array", mock)
    empty_pix = {"pixel_data": np.asarray([]).tobytes()}
    coll = dcmcoll(**{"test1": empty_pix, "test2": empty_pix, "test3": empty_pix})
    df = pd.DataFrame.from_records([{"path": dcm.filepath} for dcm in coll])
    df["score"] = 0
    splitter = JensenShannonDICOMSplitter(coll)
    run_individual_split(splitter, df)
    exp = [
        (
            "fw_gear_dicom_splitter.main",
            40,
            "Could not run JensenShannonDICOMSplitter due to missing PixelData: som",
        ),
        ("fw_gear_dicom_splitter.main", 40, "Enable debug to see full stack"),
        (
            "fw_gear_dicom_splitter.main",
            20,
            "Note: JensenShannon splitter is not necessary, but this failure "
            "indicates there is probably something wrong with your DICOM's "
            "PixelData or PixelData attributes.",
        ),
    ]
    assert caplog.record_tuples == exp


@pytest.mark.parametrize(
    "dcm_kwargs, call_counts",
    [
        (
            {
                "ImageOrientationPatient": ("DS", [0, 0, 0, 0, 0, 0]),
                "ImagePositionPatient": ("DS", [0, 0, 0]),
                "Rows": 20,
                "Columns": 20,
                "ImageType": "Localizer",
                "InstanceNumber": 10,
            },
            [2, 1, 1],
        ),
        (
            {
                "ImagePositionPatient": ("DS", [0, 0, 0]),
                "Rows": 20,
                "Columns": 20,
                "ImageType": "Localizer",
                "InstanceNumber": 10,
            },
            [1, 1, 1],
        ),
        (
            {
                "Rows": 20,
                "Columns": 20,
                "ImageType": "Localizer",
                "InstanceNumber": 10,
            },
            [0, 1, 1],
        ),
        (
            {
                "ImageOrientationPatient": ("DS", [0, 0, 0, 0, 0, 0]),
                "ImagePositionPatient": ("DS", [0, 0, 0]),
                "Rows": 20,
                "Columns": 20,
                "ImageType": "Localizer",
            },
            [0, 0, 1],
        ),
        (
            {
                "ImageOrientationPatient": ("DS", [0, 0, 0, 0, 0, 0]),
                "ImagePositionPatient": ("DS", [0, 0, 0]),
                "Columns": 20,
                "ImageType": "Localizer",
                "InstanceNumber": 10,
            },
            [2, 1, 0],
        ),
        (
            {
                "ImageOrientationPatient": ("DS", [0, 0, 0, 0, 0, 0]),
                "ImagePositionPatient": ("DS", [0, 0, 0]),
                "Rows": 20,
                "Columns": 20,
                "InstanceNumber": 10,
            },
            [2, 1, 1],
        ),
    ],
)
def test_gen_split_score(dcmcoll, mocker, dcm_kwargs, call_counts):
    coll = dcmcoll(**{f"test{i}": dcm_kwargs for i in range(3)})
    df = pd.DataFrame.from_records([{"path": dcm.filepath} for dcm in coll])
    splitter_mocks = []
    for splitter in [
        EuclideanSplitter,
        JensenShannonDICOMSplitter,
        UniqueTagSingleSplitter,
    ]:
        mock = mocker.patch(f"fw_gear_dicom_splitter.main.{splitter.__name__}")
        temp_df = df.copy(deep=True)
        temp_df["decision"] = [0.0, 0.0, 5.0]
        mock.return_value.split.return_value = temp_df
        mock.return_value.decision_val = 5
        splitter_mocks.append(mock)

    df = gen_split_score(coll)

    assert all(
        [
            mock.return_value.split.call_count == exp
            for mock, exp in zip(splitter_mocks, call_counts)
        ]
    )

    assert list(df["score"].values) == [0.0, 0.0, 1.0]


def test_run_split_localizer(dcmcoll, mocker):
    coll = dcmcoll(**{"test1": {}, "test2": {}, "test3": {}})
    split_score = mocker.patch("fw_gear_dicom_splitter.main.gen_split_score")
    df = pd.DataFrame.from_records(
        [
            {"path": dcm.filepath, "score": val}
            for dcm, val in zip(coll, [0.0, 0.0, 5.0])
        ]
    )
    split_score.return_value = df

    out = run_split_localizer(coll)
    assert len(out) == 2
    assert len(out[0]) == 2
    assert len(out[1]) == 1


def test_run_split_localizer_one_frame(dcmcoll):
    coll = dcmcoll(**{"test1": {}})
    out = run_split_localizer(coll)
    assert len(out) == 2
    assert out[0] == coll
    assert len(out[1]) == 0


def test_split_dicom_split_sopclassuid(dcmcoll, caplog):
    # GEAR-6380: Although some DICOM files are valid when multiple SeriesInstanceUIDs
    # exist (as with Secondary Capture Image Storage), the downstream gear workflow
    # does not support keeping multiple SeriesInstanceUIDs. These files should
    # be split even if they're SCIS files.
    # GEAR-6388: If max_geom_splits > 0 and one or more slices are missing ipp/iop
    # data, add_phases_to_output should return None, None and error should be logged.
    coll = dcmcoll(
        **{
            "slice1": {
                "SOPClassUID": "1.2.840.10008.5.1.4.1.1.7",
                "SeriesInstanceUID": pydicom.uid.generate_uid(),
            },
            "slice2": {
                "SOPClassUID": "1.2.840.10008.5.1.4.1.1.7",
                "SeriesInstanceUID": pydicom.uid.generate_uid(),
            },
        }
    )

    res = split_dicom(
        dcm=coll,
        group_by=["SeriesInstanceUID"],
        split_localizer=True,
        max_geom_splits=4,
    )
    assert len(res) == 2
    assert "Cannot split by geometry" in caplog.text


@pytest.mark.parametrize(
    "group_by, localizer, exp_out",
    [
        (None, False, 1),
        (None, True, 2),
        (["test"], False, 2),
        (["test"], True, 2),
    ],
)
def test_split_dicom_group_by(dcmcoll, mocker, tmp_path, group_by, localizer, exp_out):
    coll = dcmcoll(
        **{
            "test1": {"PatientID": "test1"},
            "test2": {"PatientID": "test1"},
            "test3": {"PatientID": "test1"},
            "test4": {"PatientID": "test2"},
            "test5": {"PatientID": "test2"},
            "test6": {"PatientID": "test3"},
        }
    )
    out = split_dicom(coll, ["PatientID"], False)

    assert len(out.items()) == 3
    suids = []
    for name, coll in out.items():
        suids.append(coll.get("SeriesInstanceUID"))
        if len(coll) == 3:
            assert str(name) == "series-1_MR_PatientID-test1"
        if len(coll) == 2:
            assert str(name) == "series-1000_MR_PatientID-test2"
        if len(coll) == 1:
            assert str(name) == "series-1001_MR_PatientID-test3"
    assert len(set(suids)) == 3


def test_split_dicom_group_by_two_tags(dcmcoll):
    coll = dcmcoll(
        **{
            "test1": {"PatientID": "test1", "FrameReferenceTime": 3000.0},
            "test2": {"PatientID": "test1", "FrameReferenceTime": 3000.0},
            "test3": {"PatientID": "test1", "FrameReferenceTime": 2000.0},
        }
    )
    out = split_dicom(coll, ["PatientID", "FrameReferenceTime"], False)

    assert len(out.items()) == 2
    suids = []
    for name, coll in out.items():
        suids.append(coll.get("SeriesInstanceUID"))
        if len(coll) == 2:
            assert str(name) in [
                "series-1_MR_PatientID-test1_FrameReferenceTime-3000.0",
                "series-1_MR_FrameReferenceTime-3000.0_PatientID-test1",
            ]
        if len(coll) == 1:
            assert str(name) in [
                "series-1000_MR_PatientID-test1_FrameReferenceTime-2000.0",
                "series-1000_MR_FrameReferenceTime-2000.0_PatientID-test1",
            ]
    assert len(set(suids)) == 2


def test_split_dicom_group_by_preserve_seriesinstanceuid(dcmcoll):
    # Test that SeriesInstanceUID is preserved for series
    uids = [pydicom.uid.generate_uid(), pydicom.uid.generate_uid()]
    coll = dcmcoll(
        **{
            "test1": {"PatientID": "test1", "SeriesInstanceUID": uids[0]},
            "test2": {"PatientID": "test1", "SeriesInstanceUID": uids[0]},
            "test3": {"PatientID": "test1", "SeriesInstanceUID": uids[0]},
            "test4": {"PatientID": "test1", "SeriesInstanceUID": uids[1]},
            "test5": {"PatientID": "test1", "SeriesInstanceUID": uids[1]},
            "test6": {"PatientID": "test1", "SeriesInstanceUID": uids[1]},
        }
    )
    out = split_dicom(coll, ["SeriesInstanceUID"], False)
    for name, coll in out.items():
        assert all([dcm.get("SeriesInstanceUID") in uids for dcm in coll])


def test_split_dicom_group_by_dont_preserve_seriesinstanceuid(dcmcoll):
    uids = [pydicom.uid.generate_uid(), pydicom.uid.generate_uid()]
    coll = dcmcoll(
        **{
            "test1": {"PatientID": "test1", "SeriesInstanceUID": uids[0]},
            "test2": {"PatientID": "test1", "SeriesInstanceUID": uids[0]},
            "test3": {"PatientID": "test1", "SeriesInstanceUID": uids[0]},
            "test4": {"PatientID": "test2", "SeriesInstanceUID": uids[1]},
            "test5": {"PatientID": "test2", "SeriesInstanceUID": uids[1]},
            "test6": {"PatientID": "test2", "SeriesInstanceUID": uids[1]},
        }
    )
    out = split_dicom(coll, ["PatientID"], False)
    for name, coll in out.items():
        assert not all([dcm.get("SeriesInstanceUID") in uids for dcm in coll])


def test_split_dicom_group_by_with_series(dcmcoll):
    coll = dcmcoll(
        **{
            "test1": {"PatientID": "test1", "SeriesNumber": 1},
            "test2": {"PatientID": "test1", "SeriesNumber": 1},
            "test3": {"PatientID": "test1", "SeriesNumber": 1},
            "test4": {"PatientID": "test2", "SeriesNumber": 2},
            "test5": {"PatientID": "test2", "SeriesNumber": 2},
            "test6": {"PatientID": "test3", "SeriesNumber": 2},
        }
    )
    out = split_dicom(coll, ["PatientID"], False)

    assert len(out.items()) == 3
    for name, coll in out.items():
        if len(coll) == 3:
            assert str(name) == "series-1_MR_PatientID-test1"
        if len(coll) == 2:
            assert str(name) == "series-2_MR_PatientID-test2"
        if len(coll) == 1:
            assert str(name) == "series-2_MR_PatientID-test3"


def test_split_dicom_group_by_with_series_split_on_seriesnumber(dcmcoll):
    coll = dcmcoll(
        **{
            "test1": {"PatientID": "test1", "SeriesNumber": 1},
            "test2": {"PatientID": "test1", "SeriesNumber": 1},
            "test3": {"PatientID": "test1", "SeriesNumber": 1},
            "test4": {"PatientID": "test2", "SeriesNumber": 2},
            "test5": {"PatientID": "test2", "SeriesNumber": 2},
            "test6": {"PatientID": "test3", "SeriesNumber": 2},
        }
    )
    out = split_dicom(coll, ["PatientID", "SeriesNumber"], False)

    assert len(out.items()) == 3
    for name, coll in out.items():
        if len(coll) == 3:
            assert str(name) == "series-1_MR_PatientID-test1"
        if len(coll) == 2:
            assert str(name) == "series-2_MR_PatientID-test2"
        if len(coll) == 1:
            assert str(name) == "series-2_MR_PatientID-test3"


@pytest.mark.parametrize(
    "collection_name, expected_phase_count",
    [
        ("HCC_008-CT-3-Phase-MF.dcm", 1),
        ("HCC_008-CT-3-Phase-SF.dicom.zip", 3),
    ],
)
def test_split_dicom_by_geometry(collection_name, expected_phase_count):
    collection = dicom_collection(collection_name)
    split_collections = split_dicom(
        collection, group_by="", split_localizer=False, max_geom_splits=4
    )
    assert len(split_collections) == expected_phase_count


@pytest.mark.parametrize("series", [(1, 2, 1, 3, 3), (1, 1, 1, 2, 3)])
def test_split_dicom_group_by_with_multiple_series_errors(dcmcoll, series, caplog):
    coll = dcmcoll(
        **{
            "test1": {"PatientID": "test1", "SeriesNumber": series[0]},
            "test2": {"PatientID": "test1", "SeriesNumber": series[1]},
            "test3": {"PatientID": "test1", "SeriesNumber": series[2]},
            "test4": {"PatientID": "test2", "SeriesNumber": series[3]},
            "test5": {"PatientID": "test2", "SeriesNumber": series[4]},
        }
    )
    out = split_dicom(coll, ["PatientID"], False)

    # assert not out
    # Previous behavior had no output here; GEAR-6660 updates this to
    # not return None when multiple SeriesNumbers are found.
    assert len(out) == 2
    assert "Multiple SeriesNumbers found" in caplog.text


def test_split_dicom_split_localizer(dcmcoll, mocker):
    coll = dcmcoll(
        **{
            "test1": {"Modality": "MR"},
            "test2": {"Modality": "MR"},
            "test3": {"Modality": "MR"},
        }
    )
    # df = pd.DataFrame.from_records([{"path": dcm.filepath} for dcm in coll])

    def split_localizer(archive):
        if len(archive) > 2:
            localizer = DICOMCollection()
            dicom = DICOMCollection()
            for i, dcm in enumerate(archive):
                if i < len(archive) - 1:
                    dicom.append(dcm)
                else:
                    localizer.append(dcm)
            return (dicom, localizer)

        else:
            return (archive, DICOMCollection())

    split_localizer_mock = mocker.patch(
        "fw_gear_dicom_splitter.main.run_split_localizer"
    )
    split_localizer_mock.side_effect = split_localizer

    out = split_dicom(coll, None, True)
    out_items = list(out.items())
    assert str(out_items[0][0]) == "series-1_MR"
    assert len(out_items[0][1]) == 2
    assert len(out_items[1][1]) == 1
    assert str(out_items[1][0]) == "series-1001_MR_localizer"
    assert len(out_items[1][1][0].OriginalAttributesSequence) == 1


def test_split_dicom_split_localizer_not_mrct(dcmcoll, mocker, caplog):
    coll = dcmcoll(
        **{
            "test1": {"Modality": "OCT"},
            "test2": {"Modality": "OCT"},
            "test3": {"Modality": "OCT"},
        }
    )
    # df = pd.DataFrame.from_records([{"path": dcm.filepath} for dcm in coll])
    split_localizer_mock = mocker.patch(
        "fw_gear_dicom_splitter.main.run_split_localizer"
    )
    _ = split_dicom(coll, None, True)
    split_localizer_mock.assert_not_called()
    assert (
        "Split localizer option is only applicable for MR or CT DICOMs."
        in caplog.record_tuples[-1][2]
    )


def test_split_dicom_split_localizer_has_different_seriesinstanceuid(dcmcoll, mocker):
    uids = [pydicom.uid.generate_uid(), pydicom.uid.generate_uid()]
    coll = dcmcoll(
        **{
            "test1": {"Modality": "MR", "SeriesInstanceUID": uids[0]},
            "test2": {"Modality": "MR", "SeriesInstanceUID": uids[0]},
            "test3": {"Modality": "MR", "SeriesInstanceUID": uids[0]},
            "test4": {"Modality": "MR", "SeriesInstanceUID": uids[1]},
            "test5": {"Modality": "MR", "SeriesInstanceUID": uids[1]},
        }
    )
    # df = pd.DataFrame.from_records([{"path": dcm.filepath} for dcm in coll])

    def split_localizer(archive):
        if len(archive) > 2:
            localizer = DICOMCollection()
            dicom = DICOMCollection()
            for i, dcm in enumerate(archive):
                if i < len(archive) - 1:
                    dicom.append(dcm)
                else:
                    localizer.append(dcm)
            return (dicom, localizer)
        else:
            return (archive, DICOMCollection())

    split_localizer_mock = mocker.patch(
        "fw_gear_dicom_splitter.main.run_split_localizer"
    )
    split_localizer_mock.side_effect = split_localizer
    out = split_dicom(coll, ["SeriesInstanceUID"], True)
    for name, coll in out.items():
        if name.localizer:
            assert all([dcm.get("SeriesInstanceUID") not in uids for dcm in coll])
        else:
            assert all([dcm.get("SeriesInstanceUID") in uids for dcm in coll])


def test_run_not_a_zip(tmp_path, mocker, caplog):
    sniffer = mocker.patch("fw_gear_dicom_splitter.main.sniff_dcm")
    sniffer.return_value = False
    zip_ = mocker.patch("fw_gear_dicom_splitter.main.zipfile")
    zip_.is_zipfile.return_value = False
    gear_args = GearArgs(
        dicom_path="test",
        output_dir="test",
        group_by=["test"],
        max_geom_splits=4,
        extract_localizer=True,
        zip_single=False,
        delete_input=False,
        filter_archive=True,
    )
    paths, success = run(gear_args)
    assert not paths
    assert success
    assert "Not a zip file" in caplog.record_tuples[-1][2]
    zip_.is_zipfile.assert_called_once()
    sniffer.assert_called_once()


def test_run_single_dcm(tmp_path, mocker, caplog):
    sniffer = mocker.patch("fw_gear_dicom_splitter.main.sniff_dcm")
    sniffer.return_value = True
    zip_ = mocker.patch("fw_gear_dicom_splitter.main.zipfile")
    gear_args = GearArgs(
        dicom_path="test",
        output_dir="test",
        group_by=["test"],
        max_geom_splits=4,
        extract_localizer=True,
        zip_single=False,
        delete_input=False,
        filter_archive=True,
    )
    paths, success = run(gear_args)
    assert not paths
    assert success
    assert "Input is a single DICOM" in caplog.record_tuples[-1][2]
    sniffer.assert_called_once()
    zip_.is_zipfile.assert_not_called()


@pytest.fixture
def setup_run(tmp_path, mocker, dcmcoll):
    sniffer = mocker.patch("fw_gear_dicom_splitter.main.sniff_dcm")
    zip_ = mocker.patch("fw_gear_dicom_splitter.main.zipfile")
    split = mocker.patch("fw_gear_dicom_splitter.main.split_dicom")
    add_cont = mocker.patch("fw_gear_dicom_splitter.main.add_contributing_equipment")
    dcms = mocker.patch("fw_gear_dicom_splitter.main.DICOMCollection")
    test1, test2 = MagicMock(), MagicMock()
    test1.__len__.return_value = 20
    test2.__len__.return_value = 1
    split.return_value = {"my": test1, "my2": test2}
    return zip_, split, add_cont, dcms, (test1, test2), sniffer


def test_run_zip_single(tmp_path, setup_run, dcmcoll):
    zip_, split, add_cont, dcms, files, sniffer = setup_run
    sniffer.return_value = False
    zip_.is_zipfile.return_value = True
    coll = dcmcoll(**{"test1": {}, "test2": {}})
    dcms.from_zip.return_value.__enter__.return_value = coll
    gear_args = GearArgs(
        dicom_path="test",
        output_dir=tmp_path,
        group_by=["test"],
        max_geom_splits=-1,
        extract_localizer=True,
        zip_single=True,
        delete_input=False,
        filter_archive=True,
    )
    out, success = run(gear_args)
    assert (tmp_path / "my.dicom.zip") in out
    files[0].to_zip.assert_called_once_with(tmp_path / "my.dicom.zip")
    assert (tmp_path / "my2.dicom.zip") in out
    files[1].to_zip.assert_called_once_with(tmp_path / "my2.dicom.zip")
    zip_.is_zipfile.assert_called_once()
    dcms.from_zip.assert_called_once_with("test", force=True, stop_when=None)
    split.assert_called_once_with(coll, ["test"], True, -1)
    assert add_cont.call_count == 2


def test_run_no_zip_single(tmp_path, setup_run, dcmcoll):
    zip_, split, add_cont, dcms, files, sniffer = setup_run
    sniffer.return_value = False
    zip_.is_zipfile.return_value = True
    coll = dcmcoll(**{"test1": {}, "test2": {}})
    dcms.from_zip.return_value.__enter__.return_value = coll
    gear_args = GearArgs(
        dicom_path="test",
        output_dir=tmp_path,
        group_by=["test"],
        max_geom_splits=-1,
        extract_localizer=True,
        zip_single=False,
        delete_input=False,
        filter_archive=True,
    )
    out, success = run(gear_args)
    assert (tmp_path / "my.dicom.zip") in out
    files[0].to_zip.assert_called_once_with(tmp_path / "my.dicom.zip")
    assert (tmp_path / "my2.dicom") in out
    files[1][0].save.assert_called_once_with(tmp_path / "my2.dicom")
    zip_.is_zipfile.assert_called_once()
    dcms.from_zip.assert_called_once_with("test", force=True, stop_when=None)
    split.assert_called_once_with(coll, ["test"], True, -1)
    assert add_cont.call_count == 2


def test_run_filtered_true(tmp_path, setup_run, dcmcoll, caplog):
    zip_, split, add_cont, dcms, files, sniffer = setup_run
    sniffer.return_value = False
    zip_.is_zipfile.return_value = True
    coll = dcmcoll(**{"test1": {}, "test2": {}})
    not_a_dicom = tmp_path / "some_file.txt"
    with open(not_a_dicom, "w") as f:
        f.write("This is not a zero-byte file!")
    bad_dicom = DICOM(not_a_dicom, force=True)
    coll.append(bad_dicom)
    dcms.from_zip.return_value.__enter__.return_value = coll
    gear_args = GearArgs(
        dicom_path="test",
        output_dir=tmp_path,
        group_by=["test"],
        max_geom_splits=-1,
        extract_localizer=True,
        zip_single=True,
        delete_input=False,
        filter_archive=True,
    )

    out, success = run(gear_args)
    assert "1 invalid DICOM" in caplog.text

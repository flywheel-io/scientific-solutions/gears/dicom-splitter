from unittest.mock import MagicMock

import pandas as pd
import pytest

from fw_gear_dicom_splitter.utils import (
    collection_from_df,
    collection_to_df,
    delete_input,
)


@pytest.mark.parametrize("keys", [None, ["PatientID", "ImageOrientationPatient"]])
def test_collection_to_df(dcmcoll, keys):
    coll = dcmcoll(
        **{
            "test1": {
                "PatientID": "0",
                "ImageOrientationPatient": ("DS", [0, 0, 0, 0, 0, 0]),
            },
            "test2": {
                "PatientID": "1",
                "ImageOrientationPatient": ("DS", [0, 0, 0, 0, 0, 0]),
            },
            "test3": {
                "PatientID": "2",
                "ImageOrientationPatient": ("DS", [0, 0, 0, 0, 0, 0]),
            },
        }
    )
    dataframe = collection_to_df(coll, keys)
    if keys:
        assert all([key in dataframe.columns for key in keys])
    assert dataframe.shape[0] == 3


def test_collection_from_df(dcmcoll):
    coll = dcmcoll(**{"test1": {}, "test2": {}, "test3": {}})

    dataframe = pd.DataFrame.from_records(
        [{dcm.filepath: i} for i, dcm in enumerate(coll)]
    )

    out = collection_from_df(coll, dataframe)
    assert len(out) == 3


def test_delete_input():
    """Assertion to test that delete_input() identifies and
    removes the 'dicom' input file.
    """
    context = MagicMock()
    context.get_input.return_value = {
        "hierarchy": {"type": "acquisition", "id": "test"},
        "location": {"name": "test"},
        "object": {"modality": None},
    }
    acq = MagicMock()
    file_ = MagicMock()
    file_.name = "my_acq"
    file_.file_id = "test"
    acq.get_file.return_value = file_
    context.client.get_acquisition.return_value = acq

    delete_input(context)
    acq.delete_file.assert_called_once_with(file_.name)

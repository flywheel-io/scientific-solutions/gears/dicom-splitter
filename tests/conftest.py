import pytest
from fw_file import dicom
from fw_file.dicom.testing import create_dataset
from fw_file.dicom.testing import create_dcm as fw_create_dcm

pytest_plugins = "flywheel_gear_toolkit.testing"


@pytest.fixture
def default_dcmdict():
    """default dataset dict used in create_dcm."""
    return dict(
        SOPClassUID="1.2.840.10008.5.1.4.1.1.4",  # MR Image Storage
        SOPInstanceUID="1.2.3",
        PatientID="test",
        StudyInstanceUID="1",
        SeriesInstanceUID="1.2",
    )


@pytest.fixture
def create_ds():
    """Create and return a dataset from a dcmdict."""

    return create_dataset


@pytest.fixture
def create_dcm(default_dcmdict, create_ds):  # pylint: disable=redefined-outer-name
    """Create a dataset and return it loaded as an fw_file.dicom.DICOM."""

    def dcm(file=None, preamble=None, file_meta=None, pixel_data=None, **dcmdict):
        fw_dcm = fw_create_dcm(file, preamble, file_meta, **dcmdict)
        if pixel_data is not None:
            # set on PixelData attribute instead of pixel_array to avoid validation
            fw_dcm.PixelData = pixel_data
        return fw_dcm

    return dcm


@pytest.fixture
def dcmcoll(tmp_path, create_dcm):
    """Return DICOMCollection with two files."""

    def _gen(**kwargs):
        for key, val in kwargs.items():
            create_dcm(**val, file=str(tmp_path / (key + ".dcm")))
        return dicom.DICOMCollection.from_dir(tmp_path)

    return _gen


@pytest.fixture
def jpeg2000():
    """Return a JPEG2000 array (blank img)"""
    return (
        b"\xffO\xffQ\x00/\x00\x00\x00\x00\x00\x01\x00\x00\x00\x01\x00"
        + b"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x01"
        + b"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03\x07\x01\x01\x07\x01"
        + b"\x01\x07\x01\x01\xffR\x00\x0c\x00\x00\x00\x01\x01\x05\x04\x04"
        + b"\x00\x01\xff\\\x00\x13@@HHPHHPHHPHHPHHP\xff\x90\x00\n\x00\x00"
        + b"\x00\x00\x00#\x00\x01\xff\x93\xcf\xa4\x08\x00\x80\x80\x80\x80"
        + b"\x80\x80\x80\x80\x80\x80\x80\x80\x80\x80\x80\x80\x80\xff\xd9"
    )
